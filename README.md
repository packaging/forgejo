# [Forgejo](https://forgejo.org/) Debian/Ubuntu Packages

Packages are built using [fpm](https://github.com/jordansissel/fpm/) by pushing released tags and repo is created using [Gitlabs static pages](https://morph027.gitlab.io/blog/repo-hosting-using-gitlab-pages/).

Minimum install and update tests are in place to prevent upload of broken packages.

## DEB

### Add repo signing key to apt

```
sudo curl -sfLo /etc/apt/trusted.gpg.d/morph027-forgejo.asc https://packaging.gitlab.io/forgejo/deb/gpg.key
```

### Add repo to apt

#### Old `list` Format

```
echo "deb https://packaging.gitlab.io/forgejo/deb forgejo main" | sudo tee /etc/apt/sources.list.d/morph027-forgejo.list
```

#### New `sources` deb822 Format

``` bash
sudo tee /etc/apt/sources.list.d/morph027-forgejo.sources <<EOF
Types: deb
URIs: https://packaging.gitlab.io/forgejo/deb
Suites: forgejo
Components: main
Signed-By: /etc/apt/trusted.gpg.d/morph027-forgejo.asc
EOF
```

### Install

```
sudo apt-get update
sudo apt-get install forgejo morph027-keyring
```

## RPM

### Add repo to yum/dnf

```bash
sudo tee /etc/yum.repos.d/morph027-forgejo.repo <<EOF
[forgejo]
name=morph027-forgejo
baseurl=https://packaging.gitlab.io/forgejo/rpm/$basearch
enabled=1
gpgkey=https://packaging.gitlab.io/forgejo/rpm/gpg.key
gpgcheck=1
repo_gpgcheck=1
EOF
```

### Install

```
sudo dnf install forgejo
```

## Start

```
systemctl enable --now forgejo
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
echo 'Unattended-Upgrade::Allowed-Origins {"morph027:forgejo";};' > /etc/apt/apt.conf.d/50forgejo
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
