getent passwd forgejo >/dev/null 2>&1 || adduser \
  --system \
  --shell /bin/bash \
  --gecos 'Forgejo' \
  --group \
  --disabled-password \
  --home /var/lib/forgejo \
  forgejo

if [ ! -d /etc/forgejo ]; then
  install -d -o forgejo -g forgejo -m 770 /etc/forgejo
fi
