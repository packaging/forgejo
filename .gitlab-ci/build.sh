#!/usr/bin/env bash

set -e

version=$(echo ${CI_COMMIT_TAG#*v} | cut -d'+' -f1)
patchlevel=$(echo ${CI_COMMIT_TAG} | cut -d'+' -f2)
arch="${ARCH:-amd64}"

apt-get -qq update
apt-get -qqy install ruby-dev ruby-ffi curl file
gem install dotenv -v 2.8.1 --no-doc
gem install fpm --no-doc
mkdir -p "${CI_PROJECT_DIR}/package_root/usr/bin"
curl -sfLo \
  "${CI_PROJECT_DIR}/package_root/usr/bin/forgejo" \
  "https://codeberg.org/forgejo/forgejo/releases/download/v${version}/forgejo-${version}-linux-${arch}"
file \
    --mime-type \
    --brief \
    "${CI_PROJECT_DIR}/package_root/usr/bin/forgejo" \
    | grep -q 'application/x-executable'
chmod +x "${CI_PROJECT_DIR}/package_root/usr/bin/forgejo"
fpm \
    --architecture "${ARCH}" \
    --input-type dir \
    --output-type deb \
    --package "${CI_PROJECT_DIR}/forgejo_${version}+${patchlevel}_${arch}.deb" \
    --name "${NAME}" \
    --version "${version}+${patchlevel}" \
    --description "${DESCRIPTION}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "${URL}" \
    --depends git \
    --deb-recommends morph027-keyring \
    --deb-systemd "${CI_PROJECT_DIR}/.packaging/${NAME}.service" \
    --deb-activate-noawait /etc/init.d \
    --prefix=/ \
    --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh" \
    --chdir "${CI_PROJECT_DIR}/package_root" \
    $ARTIFACTS
